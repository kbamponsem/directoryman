#include <iostream>
#include <string>
#include <fstream>
#include <typeinfo>
#include <vector>
#include <algorithm>

#include "DirectoryEntry.h"
#include "MyLinkedList.h"
#include "types.h"

using namespace std;

std::ifstream inStream;
std::ofstream outStream;


bool checkFile(std::string _fileName){
    inStream.open(_fileName);
    return inStream.fail() ? true : false;
    inStream.close();
}
vector<std::string> splitUp(std::string _data,std::string delim){

    vector<std::string> _result;
     std::size_t current, previous = 0;
    current = _data.find(delim);
    while (current != std::string::npos) {
        _result.push_back(_data.substr(previous, current - previous));
        previous = current + 1;
        current = _data.find(delim, previous);
    }
    _result.push_back(_data.substr(previous, current - previous));

    return _result;
}
void loadData( Chain<DirectoryEntry,std::string> * chain){
    std::string _data;
    vector<std::string> __result;
    DirectoryEntry _dirData;
    DirectoryApi _api;

    while(std::getline(inStream, _data)){
        __result = splitUp(_data, ",");
        cout<<"Name: "<<__result[0]<<"\nPhone Number: "<<__result[1]<<"\nAddress: "<<__result[2]<<"\nCalls Made: "<<__result[3]<<"\n"<<"Calls Received: "<<__result[4]<<"\n"<<endl;
        _api._name = __result[0];
        _api._phoneNumber = __result[1];
        _api._addr = __result[2];
        _api._callsMade = __result[3];
        _api._callsReceived = __result[4];
        _dirData.setData(&_api);

        chain->createNode(&_dirData);
    }

    
}
void fileDump(DirectoryApi *_data, std::string fileName){
    outStream.open(fileName,std::ios::app);
    outStream<<_data->_name<<","<<_data->_phoneNumber<<","<<_data->_addr<<","<<_data->_callsMade<<","<<_data->_callsReceived<<"\n";
    outStream.close();
}
std::string toLower(std::string& _data){
    std::for_each(_data.begin(),_data.end(),[](char& c){
        c=::tolower(c);
    });
    return _data;
}
void searchFunction(DirectoryEntry *head, std::string searchString){
    DirectoryEntry *_head = head;
    bool found = false;

    std::string _data = _head->getName();
    while(_head != NULL){
        std::string _data = _head->getName();

        if(toLower(searchString) == toLower(_data)){
            cout<<"Results found!!\nData :-> "<<_data<<endl;
            found = true;
        }
        
        _head = _head->getNextItem(); 
    }
    !found ?cout<<searchString<<" Not found!!\n\n" : cout<<"Found!!\n\n";
}

struct __deleteStruct<DirectoryEntry,std::string>* _searchFunction(DirectoryEntry* head, std::string searchString){
    struct __deleteStruct<DirectoryEntry,std::string> searchResult;
    DirectoryEntry *_currNode,*_prevNode=NULL;
    bool found = false;

    for(_currNode=head; _currNode!=NULL; _currNode=_currNode->getNextItem()){
        std::string _data = _currNode->getName();        
        if(toLower(searchString) == toLower(_data)){
            cout<<"Results found!!\n\nData :-> \n"<<_data<<endl;
            found = true;
            break;
        }
        cout<<"CurrNode-->"<<_currNode<<"\tPrevNode--> "<<_prevNode<<endl;
        _prevNode = _currNode;
        cout<<"CurrNode-->"<<_currNode<<"\tPrevNode--> "<<_prevNode<<endl;
    }
    
    if(!found){
       cout<<"\nItem not found!!\n\n";
       _currNode = NULL;
       _prevNode = NULL;
    }
    
    searchResult.currNode = _currNode;
    searchResult.nextNode = _currNode->getNextItem();
    searchResult.prevNode = _prevNode;
    return addressof(searchResult);
}
void deleteFunction(DirectoryEntry* head,std::string searchItem){
    cout<<"\nDelete Function\n"<<endl;
    auto searchResult = _searchFunction(head,searchItem);
    if(searchResult->currNode==NULL||searchResult->prevNode==NULL||searchResult->nextNode==NULL){
        cout<<"\nItem not found\n"<<endl;
    }else{
        searchResult->prevNode = searchResult->nextNode;
    }

}

int main(){
    std::string fileName = "linkedList.txt";
    std::string data;
    /*
    when file is found, we would want to populate current chain with details from the file.
    + how do we go about this.
    */
    DirectoryApi directory;
    DirectoryEntry _directory; 

    Chain<DirectoryEntry,std::string> chain;
    
    
    if(checkFile(fileName)){
        cout<<"File Does not exist "<<endl;
        exit(1);
    }else{
        cout<<"Records saved in the database..."<<endl;
        loadData(&chain);
        
        cout<<"---------------------------------\n\n";
        do{
        int option;
        cout<<"Welcome to DirectoryMan...\n";
        cout<<"1. Add to directory\n";
        cout<<"2. Search in directory\n";
        cout<<"3. View all directories\n";
        cout<<"4. Delete item(s)\n";
        
        cout<<"Please Wait..."<<endl;
        cin>>option;
        switch (option)
        {
        case 1:
            /* code */
            cout<<"Enter name: ";
            cin>>directory._name;
            cout<<"\n";
            cout<<"Enter phone number: ";
            cin>>directory._phoneNumber;
            cout<<"\n";
            cout<<"Enter address: ";
            cin>>directory._addr;
            cout<<"\n";
            cout<<"Number of calls received: ";
            cin>>directory._callsReceived;
            cout<<"\n";
            cout<<"Number of calls made: ";
            cin>>directory._callsMade;
            cout<<"\n"<<endl;

            _directory.setData(&directory);

            chain.createNode(&_directory);
            fileDump(&directory,fileName);
            break;
        case 2:
            cout<<"Enter search string: ";
            cin>>data;
            chain.searchNode(searchFunction,data);
            break;
        case 3:
            cout<<"---List of Records---\n"<<endl;
            chain.traverseLink(chain.getHead());
            break;
        case 4:
            cout<<"Deletion"<<endl;
            cout<<"Enter search string: ";
            cin>>data;
            chain.deleteNode(deleteFunction,data);
        default:
            break;
        }
        }while(1);
    }

    inStream.close();
    outStream.close();
}

