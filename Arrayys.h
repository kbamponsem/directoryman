#include <iostream>

int main() {
    int arr[10];
    arr[0] = 77;
 //insert 10 items
    arr[1] = 99;
    arr[2] = 44;
    arr[3] = 55;
    arr[4] = 22;
    arr[5] = 88;
    arr[6] = 11;
    arr[7] = 00;
    arr[8] = 66;
    arr[9] = 33;
    int nElems = 10;

    int _key = 11;
    std::cout<<"Array before deletion: "<<std::endl;
    for(int n:arr){
        std::cout<<n<< " ";
    }
    std::cout<<"\n";
    for(int i = 0; i<nElems; i++){
        if(_key == arr[i]) {
            for(int j =i; j<nElems; j++){
                arr[j] = arr[j+1];
            }
        }
    }

    for(int n:arr){
        std::cout<<n<< " ";
    }
 
}