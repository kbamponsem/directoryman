#include <string>
#include "MyLinkedList.h"

#ifndef DIRECTORYENTRY_H
#define DIRECTORYENTRY_H

typedef struct{
    std::string _name;
    std::string _addr;
    std::string _phoneNumber;
    std::string _callsReceived;
    std::string _callsMade;
} DirectoryApi;

class DirectoryEntry{
    std::string name;
    std::string addr;
    std::string phoneNumber;
    std::string callsReceived;
    std::string callsMade;
    DirectoryEntry* nextItem = NULL;
    friend class Chain<DirectoryEntry, std::string>;
public:
    void setData(DirectoryApi *directory);
    void printNode(DirectoryEntry *);
    DirectoryApi* getData() const;
    DirectoryEntry* getNextItem() const;
    std::string getName();
    std::string getPhoneNumber() const;
    std::string getAddr() const;
    std::string getCallsMade() const;
    std::string getCallsReceived() const;
};

std::string DirectoryEntry::getName() {
    return name;
}

std::string DirectoryEntry::getPhoneNumber() const {
    return phoneNumber;
}

std::string DirectoryEntry::getAddr() const {
    return addr;
}

std::string DirectoryEntry::getCallsMade() const {
    return callsMade;
}

std::string DirectoryEntry::getCallsReceived() const {
    return callsReceived;
}

DirectoryEntry* DirectoryEntry::getNextItem() const {
    return nextItem;
}

DirectoryApi* DirectoryEntry::getData() const{
    DirectoryApi* _data;
    _data->_name = name;
    _data->_phoneNumber = phoneNumber;
    _data->_addr = addr;
    _data->_callsMade = callsMade;
    _data->_callsReceived = callsReceived;

    return _data;
}

void DirectoryEntry::setData(DirectoryApi *directory){
    phoneNumber = directory->_phoneNumber;
    addr = directory->_addr;
    name = directory->_name;
    callsMade = directory->_callsMade;
    callsReceived = directory->_callsReceived;  
    nextItem = NULL;
}



void DirectoryEntry::printNode(DirectoryEntry *obj){
    std::cout<<"Name: "<<obj->name<<std::endl;
    std::cout<<"Phone Number: "<<obj->phoneNumber<<std::endl;
    std::cout<<"Address: "<<obj->addr<<std::endl;
    std::cout<<"Call made: "<<obj->callsMade<<std::endl;
    std::cout<<"Calls received: "<<obj->callsReceived<<std::endl;
    std::cout<<"\n";
}
#endif 