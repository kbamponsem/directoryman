#include <typeinfo>
#include <functional>
#include "types.h"

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

template <class T,typename K>
class Chain  {
    T* head = NULL;
    T* lastItem = NULL;
    T data;
    int size;
public:
    void createNode(T*);
    void traverseLink(T*);
    bool isEmpty(T*);
    T* getHead();
    void searchNode(std::function<void (T*,K)>, K);
    void deleteNode(std::function<void (Chain<T,K>*,K)>, K);
};

template<class T, class K>
void Chain<T,K>::deleteNode(std::function<void (Chain<T,K>*,K)> _deleteFunc, K searchString){
    _deleteFunc(getHead(), searchString);
}

template<class T, class K>
void Chain<T,K>::searchNode(std::function<void(T*,K)> _searchFunc, K _search){
    _searchFunc(getHead(), _search);
}

template<class T, class K>
T* Chain<T,K>::getHead(){
    return head;
}

template<class T, class K> 
void Chain<T,K>::createNode(T *obj)
{
    T *Node = new T();
    Node->setData(obj->getData());
    if (isEmpty(head)){
        head = Node;
        lastItem = Node;
        ++size;
    }
    else{
        lastItem->nextItem = Node;
        lastItem=Node; 
    }
}

template<class T,class K>
bool Chain<T,K>::isEmpty(T *head) {
    
    return head == NULL ? true : false;

}

template <class T, class K>
void Chain<T,K>::traverseLink(T *head){
    std::cout<<"\n";
    while(!isEmpty(head)){
        head->printNode(head);
        head= head->nextItem;
    }
}

#endif
