#include <stdio.h>
#define M_PI 3.142

int main(){
    double _radius;
    double _Area;
    printf("Enter radius: ");
    scanf("%lf",&_radius);

    while(_radius<0){
        printf("\nEnter radius: ");
        scanf("%lf",&_radius);
    }

    _Area = M_PI * _radius * _radius;

    printf("Area of the circle with radius %lf is %lf\n", _radius, _Area);
    
}