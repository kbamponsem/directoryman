#ifndef TYPES_H
#define TYPES_H

template<typename T, typename K>
struct __deleteStruct {
    T* currNode;
    T* prevNode;
    T* nextNode; 
};

#endif